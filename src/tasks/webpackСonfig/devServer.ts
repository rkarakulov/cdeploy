export function getDevServer (app) {
    return {
        disableHostCheck: true,
        historyApiFallback: true,
        https: true,
        port: app.port,
        inline: false,
        publicPath: '/',
        host: '0.0.0.0',
        stats: {
            chunks: false,
            assets: false
        },
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        compress: true,
        watchOptions: {
            poll: 1000,
        },
    };
}
