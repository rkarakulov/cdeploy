import {rootDir} from '../config';

export function getResolve() {
    return {
        extensions: [
            '.ts',
            '.tsx',
            '.js',
            '.json',
            '.scss',
        ],
        modules: [
            rootDir,
            'node_modules/@spotware',
            'node_modules',
        ],
    };
}
