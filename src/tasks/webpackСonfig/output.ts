import {ENVS, CODE_FILE_NAME, BUNDLE_DIR} from '../config';

export function getOutput (env, appType, gitRevisionPlugin, buildPath) {
    const output: any = {
        filename: `${CODE_FILE_NAME}.js?version=${gitRevisionPlugin.commithash()}&enabled`,
        chunkFilename: `${CODE_FILE_NAME}-[id].js?version=${gitRevisionPlugin.commithash()}`,
        sourceMapFilename: `${CODE_FILE_NAME}.js.map`,
    };
    if (env === ENVS.dev) {
        output.path = '/';
        output.publicPath = '/';
    } else {
        output.path = buildPath;
        output.publicPath = `/${BUNDLE_DIR}/`;
    }
    return output;
}
