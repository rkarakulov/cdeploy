import * as config from '../config';
import * as path from 'path';

const compassMixinsPath = path.resolve('./node_modules/compass-mixins/lib');

export function getRules(env, appType, gitRevisionPlugin) {
    const tsLoader = {
        loader: 'ts-loader',
        options: {
            transpileOnly: true,
            compilerOptions: {
                noEmitOnError: false,
                module: 'ES6',
            },
        },
    };

    const isDev = env === config.ENVS.dev;
    let fileLoaderName;
    if (isDev) {
        fileLoaderName = `${gitRevisionPlugin.commithash()}/[path][name].[ext]`;
    }
    else {
        fileLoaderName = '/[path][name].[ext]';
    }

    return [
        {
            test: /\.useable.s?css$/,
            loaders: getStyleLoaders(env, true),
        },
        {
            test: /\.s?css$/,
            exclude: /\.useable.s?css$/,
            loaders: getStyleLoaders(env, false),
        },
        {
            test: /\.tsx?$/,
            loader: tsLoader,
        },
        {
            test: /\.(jpe?g|png|gif|cur|woff|woff2|eot|ttf|mp3|ogg)$/i,
            loader: 'file-loader',
            query: {
                name: fileLoaderName,
            },
        },
        {
            test: /\.html$/i,
            loader: 'file-loader',
            query: {
                name: '/[name].[ext]',
            },
        },
        {
            test: /\.svg$/,
            loader: 'svg-inline-loader',
        },
    ];
}

function getStyleLoaders(env, isUseable: boolean) {
    const addSourceMaps = env === config.ENVS.debug || env === config.ENVS.dev;

    const result = [
        {
            loader: `style-loader${ isUseable
                ? '/useable'
                : ''}`,
            query: {
                sourceMap: addSourceMaps,
                convertToAbsoluteUrls: addSourceMaps,
            },
        },
        {
            loader: 'css-loader',
            query: {
                sourceMap: addSourceMaps,
            },
        },
        {
            loader: 'sass-loader',
            query: {
                sourceMap: addSourceMaps,
                includePaths: [
                    compassMixinsPath,
                ],
            },
        },
    ];

    if (isUseable) {
        result.push({
            loader: 'text-transform-loader',
            options: {
                prependText: '.useable-style-loader-loaded { float: left; display: none; }',
            },
        } as any);
    }

    return result;
}
