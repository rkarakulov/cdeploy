import {sourceMapTypes, ENVS} from '../config';

export function getDevTool(env) {
    switch (env) {
        case ENVS.debug:
            return sourceMapTypes.inlineSourceMap;
        case ENVS.release:
            return sourceMapTypes.hiddenSourceMap;
        default:
            return sourceMapTypes.eval;
    }
}
