import * as path from 'path';

import * as ProvidePlugin from 'webpack/lib/ProvidePlugin';
import * as DefinePlugin from 'webpack/lib/DefinePlugin';
import * as WebpackNotifierPlugin from 'webpack-notifier';
import * as CopyWebpackPlugin from 'copy-webpack-plugin';
import * as HtmlWebpackPlugin from 'html-webpack-plugin';
// @ts-ignore
import * as ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
import * as DuplicatePackageCheckerPlugin from 'duplicate-package-checker-webpack-plugin';
import * as FilterWarningsPlugin from 'webpack-filter-warnings-plugin';
import {getDefaultSpinnerStyle} from '../helpers';
import {ENVS} from '../config';

const brandingPath = path.resolve(__dirname, '../../node_modules/branding');
const templatesPath = path.resolve(__dirname, '../templates');

export function getPlugins(app, gitRevisionPlugin, env) {
    const mode = env.mode;
    return [
        ...getCommonPlugins(app, gitRevisionPlugin, env),
        ...getDevPlugins(app, gitRevisionPlugin, mode, app.template),
        ...getProductionPlugins(),

        // see https://github.com/TypeStrong/ts-loader
        //If you enable transpileOnly option in ts-loader config, webpack 4 will give you "export not found"
        // warnings any time you re-export a type
        // The reason this happens is that when typescript doesn't do a full type check, it does not have enough
        // information to determine whether an imported name is a type or not, so when the name is then exported,
        // typescript has no choice but to emit the export. Fortunately, the extraneous export should not be harmful,
        // so you can just suppress these warnings:
        new FilterWarningsPlugin({
            exclude: /export .+? was not found in/,
        })
    ];
}

function getCommonPlugins(app, gitRevisionPlugin, env) {
    const mode = env.mode;
    const providePlugin = new ProvidePlugin({
        _: 'lodash',
        $: 'jquery',
        jQuery: 'jquery',
    });
    const webpackNotifierPlugin = new WebpackNotifierPlugin({
        alwaysNotify: true,
    });

    const brokersPackageJson = require(path.resolve(brandingPath, 'package.json'));
    const brokersVersion = brokersPackageJson.version;

    const definePluginParams = {
        'BUNDLE_VERSION': JSON.stringify(gitRevisionPlugin.commithash()),
        'BROKERS_VERSION': JSON.stringify(brokersVersion),
        'FORCE_DEBUG_MODE': JSON.parse(JSON.stringify(mode === ENVS.dev || mode === ENVS.debug)),
        'APP_NAME': JSON.stringify(app.name),
    };
    if (mode !== ENVS.dev) {
        definePluginParams['process.env.NODE_ENV'] = JSON.stringify('production');
    }
    else {
        definePluginParams['process.env.NODE_ENV'] = JSON.stringify(ENVS.dev);
    }
    const definePlugin = new DefinePlugin(definePluginParams);
    const duplicateCheckPlugin = new DuplicatePackageCheckerPlugin({
        verbose: true,
        emitError: false,
        exclude: instance => {
            const forExclude = [
                'hash-base',
                'inherits',
                'isarray',
            ];
            return !!forExclude.find(excludeName => {
                return excludeName === instance.name;
            });
        },
    });
    const forkTsCheckerPlugin = new ForkTsCheckerWebpackPlugin({
        //tslint: './.tslintrc.js', // now it's getting error from tests
        //blockEmit: true,
        workers: ForkTsCheckerWebpackPlugin.TWO_CPUS_FREE,
    });

    return [
        providePlugin,
        webpackNotifierPlugin,
        definePlugin,
        duplicateCheckPlugin,
        forkTsCheckerPlugin,
    ];
}

function getDevPlugins(app, gitRevisionPlugin, env, template) {
    if (env !== ENVS.dev) {
        return [];
    }
    return [
        gitRevisionPlugin,
        new HtmlWebpackPlugin({
            template: path.resolve(templatesPath, template),
            filename: 'index.html',
            spinnerCSS: getDefaultSpinnerStyle(),
        }),
        new CopyWebpackPlugin([
            {
                from: brandingPath,
                to: 'branding',
            },
        ]),
    ];
}

function getProductionPlugins() {
    return [];
}
