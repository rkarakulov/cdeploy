import {ENVS, appNames} from '../config';

export const apps = {
    [appNames.crypto]: {
        port: 8181,
        name: appNames.crypto,
    },
    [appNames.storybook]: {
        port: 8989,
        name: appNames.storybook,
    },
};

export function getApp(appName, env) {
    const app: any = apps[appName];
    const mode = env === ENVS.dev
        ? 'dev'
        : 'prod';
    app.entry = `${app.name}_web.tsx`;
    app.template = `${app.name}_web_${mode}.ejs`;
    return app;
}
