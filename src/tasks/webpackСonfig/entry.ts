import * as path from 'path';
const polifills = [
    'core-js/modules/es6.object.assign',
    'core-js/modules/es6.symbol',
];

export function getEntry (app) {
    const entryLast = path.resolve(__dirname, '../entries', app.entry);
    const entry = [entryLast];

    return polifills.concat(entry);
}
