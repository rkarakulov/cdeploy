import {colors, log} from 'gulp-util';

class Logger {
    private static instance: Logger = null;

    public static getInstance(): Logger {
        Logger.instance = Logger.instance
            ? Logger.instance
            : new Logger();
        return Logger.instance;
    }

    public error(error: string) {
        log(colors.red(error));
    }

    public success(message: string) {
        log(colors.green(message));
    }

    public info(message: string) {
        log(colors.blue(message));
    }

    public assert(param, message: string) {
        if (!param) {
            this.info(message);
        }
    }

    public errorHandler(title: string) {
        return err => {
            log(colors.red(title), err.toString());
        };
    }
}

export const logger: Logger = Logger.getInstance();
