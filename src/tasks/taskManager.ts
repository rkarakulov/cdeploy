import * as async from 'async';
import * as path from 'path';
import * as gulpUtil from 'gulp-util';
// @ts-ignore
import * as fsFinder from 'fs-finder';
import {logger} from './logger';
import {ENVS, paths} from './config';

export function getTask(taskName, ...params) {
    const taskFilePath = taskName.replace(/:/g, '/');
    return getTaskByPath(taskFilePath, ...params);
}

export function chainTask(taskName, ...params) {
    if (typeof taskName === 'function') {
        return (results, next) => taskName(...params)(next);
    }

    return (results, next) => getTask(taskName, ...params)(next);
}

export function getInlinePatams(paramsNames, callback) {
    return callback(null, getInlinePatamsSync(paramsNames));
}

export function getInlinePatamsWithValidation(paramsNames, callback) {
    async.auto({
        validation: next => ensureInlineParams(paramsNames, next),
        params: [
            'validation',
            (results, next) => {
                const inlineParams = getInlinePatamsSync(paramsNames);
                next(null, inlineParams);
            },
        ],
    }, 4, (error, results) => callback(error, results.params));
}

export function getInlinePatamsSync(paramsNames) {
    const result = {};
    paramsNames.forEach(paramName => (result[paramName] = getInlineParam(paramName)));
    return result;
}

export function ensureInlineParams(paramsNames, callback) {
    const params = Array.isArray(paramsNames)
        ? paramsNames
        : [paramsNames];
    const missingParams = params.filter(param => (getInlineParam(param) === undefined));

    if (missingParams.length) {
        missingParams.forEach(missingParam => {
            const errorMessage = `Missing required parameter: ${ missingParam } `;
            logger.error(errorMessage);
        });
        return callback(new Error('Missing required parameters'));
    }

    return callback();
}

export function getInlineParam(paramName) {
    return gulpUtil.env[paramName];
}

export function validateEnv(env, callback) {
    const validEnvValues = Object.keys(ENVS)
                                 .map(key => ENVS[key]);
    if (validEnvValues.indexOf(env) === -1) {
        return callback(new Error(`Invalid "env" parameter value. Should be one of: ${ validEnvValues.join(', ') }`));
    }
    return callback();
}

function getTaskByPath(filePath, ...params) {
    const taskName = path.basename(filePath);
    const taskPath = path.resolve(paths.tasks, path.dirname(filePath));

    const taskFilePath = fsFinder.in(taskPath)
                                 .findFiles(`${ taskName }.ts`)
                                 .shift();

    const task = require(taskFilePath);
    return task(...params);
}
