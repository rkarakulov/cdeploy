import * as config from './config';
import * as GitRevisionPlugin from 'git-revision-webpack-plugin';
import {getApp} from './webpackСonfig/apps';
import {getRules} from './webpackСonfig/rules';
import {getEntry} from './webpackСonfig/entry';
import {getPlugins} from './webpackСonfig/plugins';
import {getDevServer} from './webpackСonfig/devServer';
import {getResolve} from './webpackСonfig/resolve';
import {getDevTool} from './webpackСonfig/devTool';
import {getOutput} from './webpackСonfig/output';

const gitRevisionPlugin = new GitRevisionPlugin();

/**
 *
 * @param env {
 *  hot: boolean, - hot module reloading for react
 *  app: 'integrated', - app name that we want to start
 *  appType: 'web' - it uses only with trader app. default - 'web'
 *  mode: 'debug' | 'release' | 'dev', - debug - build for production with sourcemaps,
 *      release - build for production without sourcemaps, dev - build for developers(sourcemaps, devserver, etc.).
 *     default - 'dev' buildPath: string, - uses in debug and release mode
 * }
 * @returns Object - webpack config
 */
module.exports = env => {
    env.mode = env.mode || config.ENVS.dev;
    const app = getApp(env.app, env.mode);

    const res: any = {
        mode: env.mode === 'release'
            ? 'production'
            : 'development',
        performance: {
            hints: false,
        },
        resolve: getResolve(),
        devtool: getDevTool(env.mode),
        node: {
            dns: 'mock',
            net: 'mock',
            fs: 'empty',
        },
        output: getOutput(env.mode, env.appType, gitRevisionPlugin, env.buildPath),
        entry: getEntry(app),
        module: {
            rules: getRules(env.mode, env.appType, gitRevisionPlugin),
        },
        plugins: getPlugins(app, gitRevisionPlugin, env),
    };

    if (env.mode === config.ENVS.dev) {
        res.devServer = getDevServer(app);
    }

    return res;
};
