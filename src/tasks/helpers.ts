import * as fs from 'fs';
import * as path from 'path';

export function getDefaultSpinnerStyle() {
    const pathToCss = path.resolve(__dirname, './_common/spinner.css');
    const fileContent = fs.readFileSync(pathToCss, 'utf8');
    return fileContent;
}
