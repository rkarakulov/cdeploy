import * as path from 'path';

export const GIT_MODIFIED_FILTER = ['M', 'MM'];
export const CODE_FILE_NAME = 'bundle';
export const BUNDLE_DIR = 'bundle';

export const sourceMapTypes = {
    inlineSourceMap: 'inline-source-map',
    hiddenSourceMap: 'hidden-source-map',
    eval: 'eval'
};

export const lintConfigFile = '.tslintrc.js';
export const lintConfigFileFull = '.tslintrc.full.js';

export const canonicalUrl = {
    FxPro: 'https://ct.fxpro.com/',
    fxpromarkets: 'https://markets.fxpro.com/'
};

export const INLINE_PARAMS_NAMES = {
    env: 'env',
    brokersTargetPath: 'brokers-target',
    bundleTargetPath: 'bundle-target',
    appName: 'app-name'
};

export const ENVS = {
    debug: 'debug',
    release: 'release',
    dev: 'dev'
};

const ALL_TS_FILES = [
    './apps/*/src/**/*.+(ts|tsx)',
    './packages/*/src/**/*.+(ts|tsx)',
    './tasks/**/*.+(ts|tsx)',
    '!**/*-protobuf.ts'
];
const ALL_TS_FILES_NO_TESTS = [
    ...ALL_TS_FILES,
    '!**/*.mock.ts',
    '!**/*.test.ts',
];
const ALL_TS_FILES_NO_TESTS_NO_DESCRIPTION = [
    ...ALL_TS_FILES_NO_TESTS,
    '!**/*.d.ts'
];

export const filters = {
    ALL_TS_FILES,
    ALL_TS_FILES_NO_TESTS,
    ALL_TS_FILES_NO_TESTS_NO_DESCRIPTION,
};

export const paths = {
    tasks: './'
};

export const appNames = {
    crypto: 'crypto',
    storybook: 'storybook',
};

export const rootDir = path.resolve(__dirname, '../packages/ctrader-all/src');
export const packagesDir = path.resolve(__dirname, '../packages');
